var cheerio = require('cheerio')
var path = require('path')

// Read the HTML report
fs = require('fs')
var htmlContent;
try {
    htmlContent = fs.readFileSync(process.argv[2], 'utf8');
} catch(e) {
    console.log('Error:', e.stack);
    process.exit(1);
}

// Get the directory containing the results to make pathes relative to it later.
report_directory = path.dirname(process.argv[2])

const $ = cheerio.load(htmlContent)

// Map that keeps the total tally of the license occurrences
var licenses = {};

// Extract dependencies info.
var dependencies = []
$('div.dependencies div').each(function(i, doc) {
    // Get license name.
    license = $(this).find('blockquote p').text().trim();
    license = license.split("\n")[0];

    if(licenses[license]) {
        licenses[license].count += 1;
    } else {
        licenses[license] = { count: 1, name: license }
    }

    // Get URL.
    license_url = $(this).find('blockquote p a[href]').attr('href');

    // Get dependency name.
    dependency_name = $(this).find('h2').text().trim();
    dependency_name = dependency_name.split("\n")[0];

    // Get dependency URL.
    dependency_url = $(this).find('h2 a[href]').attr('href');

    // Get dependency description.
    dependency_description = $(this).find('dl').first().next().text().trim();

    // Get dependency location relative to the project root path
    dependency_pathes = []
    $(this).find('dl').first().find('dd').each(function(i, doc) {
        dependency_path = path.relative(report_directory, $(this).text().trim());

        // Whitespace path means current directory
        if (!dependency_path) {
            dependency_path = ".";
        }

        dependency_pathes.push(dependency_path);
    })
    dependencies.push({
        license: {
            name: license,
            url: license_url
        },
        dependency: {
            name: dependency_name,
            url: dependency_url,
            description: dependency_description,
            pathes: dependency_pathes
        }
    })
})

// Stable sort of licenses. First license count descending, then license name ascending
licenses = Object.values(licenses)
  .sort(function (a, b) {
  if (a.count === b.count) {
    return a.name > b.name ? 1 : -1;
  }
  return a.count < b.count ? 1 : -1;
});

console.log(JSON.stringify({
    licenses: licenses,
    dependencies: dependencies}, null, 4))



